FILE = "output21.xml"
import pprint
from bs4 import BeautifulSoup

pp = pprint.PrettyPrinter(indent=4)
soup = BeautifulSoup(open(FILE).read(),"lxml")
'''
The findAll method traverses the tree, starting at the given point, 
and finds all the Tag and NavigableString objects that match the criteria you give. 
'''
links = soup.findAll('l') 

pp.pprint(links[0].findAll('seg')) #Each list is an bs4 element tag 
print('\n')
# pp.pprint(links[0].findAll('seg')[0]) #Each list is an bs4 element tag
# pp.pprint(links[0].findAll('seg')[0].string) #Each list is an bs4 element tag
# pp.pprint(links[0].findAll('seg')[1].string) #Each list is an bs4 element tag
# pp.pprint(links[0].findAll('seg')[2].string) #Each list is an bs4 element tag

for seg in links[0].findAll('seg'):
	pp.pprint(seg.string)