from mongoengine import *

'''
Drop all colleciton stored previously 
'''
def dropAll(*collection):
	[coll.drop_collection() for coll in collection]	

db = connect('nicholasDB',host='saab.ischool.utexas.edu',username='usernicholas',password='niclam')

class Segment(Document):
	seg_order = IntField()
	seg_type = StringField(max_length=50)
	me_root = StringField(max_length=50)
	pos = StringField(max_length=50)
	name = StringField(max_length=50)

class Output(Document):
	lineNumber = IntField()		
	seg = ListField(ReferenceField(Segment))

dropAll(Segment,Output)

res1 = Segment(seg_order=1,seg_type="word",me_root="interj",name="I").save()
res2 = Segment(seg_order=2,seg_type="word",me_root="litel",name="will").save()
res3 = Segment(seg_order=3,seg_type="word",me_root="verb",name="noScope").save()
res4 = Segment(seg_order=4,seg_type="word",me_root="wwar",name="you").save()
res5 = Segment(seg_order=5,seg_type="word",me_root="awerab",name="tonight").save()

output1 = Output(lineNumber=1,seg=[res1,res2,res3,res4,res5])
output1.save()

for output in Output.objects:
	print(output.lineNumber)	
	[print (i.name) for i in output.seg]